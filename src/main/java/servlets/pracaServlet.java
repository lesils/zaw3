package servlets;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.Dzielo;
import main.Wynagrodzenie;
import main.Zlecenie;

@WebServlet("/praca")
public class pracaServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Integer rok = Integer.parseInt(request.getParameter("rok"));
		Double kwota = Double.parseDouble(request.getParameter("kwota"));
		String rodzajUmowy = request.getParameter("rodzajUmowy");
		String typKwoty = request.getParameter("typKwoty");
		Wynagrodzenie wynagrodzenie = new Wynagrodzenie(rok, kwota, typKwoty);
		
		
		if (rodzajUmowy.equalsIgnoreCase("dzielo")){
			Double kosztyPrzychodu = Double.parseDouble(request.getParameter("kosztyPrzychodu"));
			Dzielo dzielo = new Dzielo(rok, kwota, typKwoty, kosztyPrzychodu);
		}

		if (rodzajUmowy.equalsIgnoreCase("zlecenie")){
			Double kosztyPrzychodu = Double.parseDouble(request.getParameter("kosztyPrzychodu"));
			String skladkaRentowa = request.getParameter("rentowa");
			String skladkaEmerytalna = request.getParameter("emerytalna");
			String skladkaChorobowa = request.getParameter("chorobowa");
			String skladkaZdrowotna = request.getParameter("zdrowotna");
			Zlecenie zlecenie = new Zlecenie(rok, kwota, typKwoty, kosztyPrzychodu, skladkaRentowa,
					skladkaEmerytalna, skladkaChorobowa, skladkaZdrowotna);

		}
		//if ((request.getParameter("wylicz") != null)){
		StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		sb.append("<head>");
		sb.append("<style> th, td {padding: 5px;text-align: left; border: 1px solid black;}</style>");
		sb.append("</head>");
		sb.append("<body>");		
		sb.append("<h1>Wynagrodzenie</h1>");
		if (rodzajUmowy.equals("dzielo")){
		sb.append(createTableDzielo(kwota));
		}
		if (rodzajUmowy.equals("zlecenie")){
			sb.append(createTableZlecenie(kwota));
			}
		sb.append("</body>");
		sb.append("</html>");	
		response.getWriter().print(sb);
	}
	public StringBuilder createTableDzielo(double kwota){
		StringBuilder sb = new StringBuilder();
		//if (kwotaPrzychodu.contains("tak")){}	
	double	kwotaPrzychodu = kwota*0.2;
	double  podOpodatkowania = kwota-kwotaPrzychodu;
	double  podatek = podOpodatkowania*0.18;
	double  netto = kwota-podatek;
		sb.append("<table class='Harmonogram' style='border: 1px solid black;'>");
		sb.append("<tr>");
		sb.append("<th>Brutto</th>");
		sb.append("<th>Koszt uzyskania przychodu</th>");
		sb.append("<th>Podstawa opodatkowania</th>");
		sb.append("<th>Zaliczka na pit</th>");
		sb.append("<th>Netto</th>");
		sb.append("</tr>");
		sb.append("<tr>");
	    sb.append("<td>");
	    sb.append(kwota);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(kwotaPrzychodu);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(podOpodatkowania);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(podatek);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(netto);
	    sb.append("</td>");
		sb.append("</tr>");
		
		return sb;
	}
	public StringBuilder createTableZlecenie(double kwota){
		StringBuilder sb = new StringBuilder();
	double	kwotaPrzychodu = kwota*0.2;
	double  podOpodatkowania = kwota-kwotaPrzychodu;
	double  podatek = podOpodatkowania*0.18;
	double  netto = kwota-podatek;
	double emerytalna = kwota * 00.0976;
	double rentowa= kwota * 0.015;
	double chorobowe = kwota * 0.245;
	double zdrowotne = kwota *0.075;
	
		sb.append("<table class='Harmonogram' style='border: 1px solid black;'>");
		sb.append("<tr>");
		sb.append("<th>Brutto</th>");
		sb.append("<th>emerytalne</th>");
		sb.append("<th>rentowe</th>");
		sb.append("<th>chorobowe</th>");
		sb.append("<th>zdrowotne</th>");
		sb.append("<th>koszt uzyskania przcyhodu</th>");
		sb.append("<th>Podstawa opodatkowania</th>");
		sb.append("<th>Zaliczka na pit</th>");
		sb.append("<th>netto</th>");
		sb.append("</tr>");
		sb.append("<tr>");
	    sb.append("<td>");
	    sb.append(kwota);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(emerytalna);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(rentowa);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(chorobowe);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(zdrowotne);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(kwotaPrzychodu);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(podOpodatkowania);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(podatek);
	    sb.append("</td>");
	    sb.append("<td>");
	    sb.append(netto);
	    sb.append("</td>");
		sb.append("</tr>");
		
		return sb;
	
	}
}
