package main;


public class Praca {


    private String skladkaRentowa;
    private String skladkaEmerytalna;
    private String skladkaChorobowa;
    private String skladkaZdrowotna;

  


    public String getSkladkaRentowa() {
        return skladkaRentowa;
    }

    public void setSkladkaRentowa(String skladkaRentowa) {
        this.skladkaRentowa = skladkaRentowa;
    }

    public String getSkladkaEmerytalna() {
        return skladkaEmerytalna;
    }

    public void setSkladkaEmerytalna(String skladkaEmerytalna) {
        this.skladkaEmerytalna = skladkaEmerytalna;
    }

    public String getSkladkaChorobowa() {
        return skladkaChorobowa;
    }

    public void setSkladkaChorobowa(String skladkaChorobowa) {
        this.skladkaChorobowa = skladkaChorobowa;
    }

    public String getSkladkaZdrowotna() {
        return skladkaZdrowotna;
    }

    public void setSkladkaZdrowotna(String skladkaZdrowotna) {
        this.skladkaZdrowotna = skladkaZdrowotna;
    }
    
    
}