package main;

public class Wynagrodzenie {
	private int rok;
	private double kwotaWynagrodzenia;
	private String typKwoty;

	public Wynagrodzenie(int rok, double kwotaWynagrodzenia, String typKwoty) {
		this.rok = rok;
		this.kwotaWynagrodzenia = kwotaWynagrodzenia /100;
		this.typKwoty = typKwoty;
	}

	public double getKwotaWynagrodzenia() {
		return kwotaWynagrodzenia;
	}

	public void setKwotaWynagrodzenia(double kwotaWynagrodzenia) {
		this.kwotaWynagrodzenia = kwotaWynagrodzenia;
	}

	public int getRok() {
		return rok;
	}

	public void setRok(int rok) {
		this.rok = rok;
	}

	public String getTypKwoty() {
		return typKwoty;
	}

	public void setTypKwoty(String typKwoty) {
		this.typKwoty = typKwoty;
	}

}
